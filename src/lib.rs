#![allow(deprecated)]
#![allow(dead_code)]

#[macro_use]
extern crate glib;
extern crate bitflags;
extern crate glib_sys;
extern crate gobject_sys;
extern crate gio_sys;
extern crate gtk_sys;
extern crate emeus_sys;

extern crate lazy_static;
extern crate libc;

extern crate gtk;
extern crate gio;


mod auto;
pub use self::auto::*;